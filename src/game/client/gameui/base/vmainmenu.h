//========= Copyright � 1996-2008, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=====================================================================================//

#ifndef VMAINMENU_H
#define VMAINMENU_H

#include "basemodui.h"
#include "vflyoutmenu.h"
#include "steam/steam_api.h"

namespace BaseModUI 
{
	class MainMenu : public CBaseModFrame, public IBaseModFrameListener, public FlyoutMenuListener
	{
		DECLARE_CLASS_SIMPLE( MainMenu, CBaseModFrame );

	public:
		MainMenu( vgui::Panel *parent, const char *panelName );
		~MainMenu();

		void UpdateVisibility();

		//flyout menu listener
		virtual void OnNotifyChildFocus( vgui::Panel* child ) {}
		virtual void OnFlyoutMenuClose( vgui::Panel* flyTo ) {}
		virtual void OnFlyoutMenuCancelled() {}

	protected:
		virtual void ApplySchemeSettings( vgui::IScheme *pScheme );
		virtual void OnCommand( const char *command );
		virtual void OnKeyCodePressed( vgui::KeyCode code );
		virtual void OnKeyCodeTyped( vgui::KeyCode code );
		virtual void OnThink();
		virtual void OnOpen();
		virtual void RunFrame();
		virtual void PaintBackground() {}

	private:

		static void AcceptQuitGameCallback();
		void SetFooterState() {}
	};
}

#endif // VMAINMENU_H
