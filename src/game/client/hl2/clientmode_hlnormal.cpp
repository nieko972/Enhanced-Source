//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ====
//
// Purpose: 
//
//=============================================================================
#include "cbase.h"
#include "clientmode_hlnormal.h"
#include "vgui_int.h"
#include "hud.h"
#include <vgui/IInput.h>
#include <vgui/IPanel.h>
#include <vgui/ISurface.h>
#include "view_shared.h"
#include <vgui_controls/AnimationController.h>
#include "iinput.h"
#include "input.h"
#include "vgui_int.h"
#include "ienginevgui.h"
#include "cdll_client_int.h"
#include "c_basehlplayer.h"
#include "engine/IEngineSound.h"
#include "ivmodemanager.h"
#include "panelmetaclassmgr.h"
#include "viewpostprocess.h"
#include "shaderapi/ishaderapi.h"
#include "tier2/renderutils.h"
#include "materialsystem/imaterial.h"
#include "materialsystem/imaterialvar.h"
#include "object_motion_blur_effect.h"
#include "glow_outline_effect.h"
#include "backgroundvideo.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

extern ConVar mat_object_motion_blur_enable;

extern bool g_bRollingCredits;

ConVar default_fov( "default_fov", "75", FCVAR_CHEAT );
ConVar fov_desired( "fov_desired", "75", FCVAR_ARCHIVE | FCVAR_USERINFO, "Sets the base field-of-view.", true, 75.0, true, 90.0 );

vgui::HScheme g_hVGuiCombineScheme = 0;

static IClientMode *g_pClientMode[ MAX_SPLITSCREEN_PLAYERS ];
IClientMode *GetClientMode()
{
	ASSERT_LOCAL_PLAYER_RESOLVABLE();
	return g_pClientMode[ GET_ACTIVE_SPLITSCREEN_SLOT() ];
}

// Voice data
void VoxCallback( IConVar *var, const char *oldString, float oldFloat )
{
	if ( engine && engine->IsConnected() )
	{
		ConVarRef voice_vox( var->GetName() );
		if ( voice_vox.GetBool() /*&& voice_modenable.GetBool()*/ )
		{
			engine->ClientCmd( "voicerecord_toggle on\n" );
		}
		else
		{
			engine->ClientCmd( "voicerecord_toggle off\n" );
		}
	}
}
ConVar voice_vox( "voice_vox", "0", FCVAR_ARCHIVE, "Voice chat uses a vox-style always on", false, 0, true, 1, VoxCallback );

//--------------------------------------------------------------------------------------------------------
class CVoxManager : public CAutoGameSystem
{
public:
	CVoxManager() : CAutoGameSystem( "VoxManager" ) { }

	virtual void LevelInitPostEntity( void )
	{
		if ( voice_vox.GetBool() /*&& voice_modenable.GetBool()*/ )
		{
			engine->ClientCmd( "voicerecord_toggle on\n" );
		}
	}


	virtual void LevelShutdownPreEntity( void )
	{
		if ( voice_vox.GetBool() )
		{
			engine->ClientCmd( "voicerecord_toggle off\n" );
		}
	}
};


//--------------------------------------------------------------------------------------------------------
static CVoxManager s_VoxManager;

// --------------------------------------------------------------------------------- //
// CHLModeManager.
// --------------------------------------------------------------------------------- //

class CHLModeManager : public IVModeManager
{
public:
	virtual void	Init();
	virtual void	SwitchMode( bool commander, bool force ) {}
	virtual void	LevelInit( const char *newmap );
	virtual void	LevelShutdown( void );
	virtual void	ActivateMouse( bool isactive ) {}
};


static CHLModeManager g_ModeManager;
IVModeManager *modemanager = ( IVModeManager * )&g_ModeManager;
// --------------------------------------------------------------------------------- //
// CASWModeManager implementation.
// --------------------------------------------------------------------------------- //

#define SCREEN_FILE		"scripts/vgui_screens.txt"

void CHLModeManager::Init()
{
	for( int i = 0; i < MAX_SPLITSCREEN_PLAYERS; ++i )
	{
		ACTIVE_SPLITSCREEN_PLAYER_GUARD( i );
		g_pClientMode[ i ] = GetClientModeNormal();
	}

	PanelMetaClassMgr()->LoadMetaClassDefinitionFile( SCREEN_FILE );
}

void CHLModeManager::LevelInit( const char *newmap )
{
	for( int i = 0; i < MAX_SPLITSCREEN_PLAYERS; ++i )
	{
		ACTIVE_SPLITSCREEN_PLAYER_GUARD( i );
		GetClientMode()->LevelInit( newmap );
	}
}

void CHLModeManager::LevelShutdown( void )
{
	for( int i = 0; i < MAX_SPLITSCREEN_PLAYERS; ++i )
	{
		ACTIVE_SPLITSCREEN_PLAYER_GUARD( i );
		GetClientMode()->LevelShutdown();
	}
}

ClientModeHLNormal g_ClientModeNormal[ MAX_SPLITSCREEN_PLAYERS ];
IClientMode *GetClientModeNormal()
{
	Assert( engine->IsLocalPlayerResolvable() );
	return &g_ClientModeNormal[ engine->GetActiveSplitScreenPlayerSlot() ];
}

ClientModeHLNormal* GetClientModeHLNormal()
{
	Assert( engine->IsLocalPlayerResolvable() );
	return &g_ClientModeNormal[ engine->GetActiveSplitScreenPlayerSlot() ];
}

//-----------------------------------------------------------------------------
// Purpose: this is the viewport that contains all the hud elements
//-----------------------------------------------------------------------------
class CHudViewport : public CBaseViewport
{
private:
	DECLARE_CLASS_SIMPLE( CHudViewport, CBaseViewport );

protected:
	virtual void ApplySchemeSettings( vgui::IScheme *pScheme )
	{
		BaseClass::ApplySchemeSettings( pScheme );

		GetHud().InitColors( pScheme );

		SetPaintBackgroundEnabled( false );
	}

	virtual void CreateDefaultPanels( void ) { /* don't create any panels yet*/ };
};


bool ClientModeHLNormal::ShouldDrawCrosshair( void )
{
	return ( g_bRollingCredits == false );
}

class FullscreenHLViewport : public CHudViewport
{
private:
	DECLARE_CLASS_SIMPLE( FullscreenHLViewport, CHudViewport );

private:
	virtual void InitViewportSingletons( void )
	{
		SetAsFullscreenViewportInterface();
	}
};

class ClientModeHLNormalFullscreen : public	ClientModeHLNormal
{
	DECLARE_CLASS_SIMPLE( ClientModeHLNormalFullscreen, ClientModeHLNormal );
public:
	virtual void InitViewport()
	{
		// Skip over BaseClass!!!
		BaseClass::InitViewport();
		m_pViewport = new FullscreenHLViewport();
		m_pViewport->Start( gameuifuncs, gameeventmanager );
	}

	virtual void Init()
	{
		// Skip over BaseClass!!!
		BaseClass::Init();

		// Load up the combine control panel scheme
		if ( !g_hVGuiCombineScheme )
		{
			g_hVGuiCombineScheme = vgui::scheme()->LoadSchemeFromFileEx( enginevgui->GetPanel( PANEL_CLIENTDLL ), IsXbox() ? "resource/ClientScheme.res" : "resource/CombinePanelScheme.res", "CombineScheme" );
			if ( !g_hVGuiCombineScheme )
				Warning( "Couldn't load combine panel scheme!\n" );
		}
	}
	void Shutdown()	{}
};

//--------------------------------------------------------------------------------------------------------
static ClientModeHLNormalFullscreen g_FullscreenClientMode;
IClientMode *GetFullscreenClientMode( void )
{
	return &g_FullscreenClientMode;
}

ClientModeHLNormal::ClientModeHLNormal()
{
	m_pCurrentPostProcessController = NULL;
	m_PostProcessLerpTimer.Invalidate();
	m_pCurrentColorCorrection = NULL;
}

ClientModeHLNormal::~ClientModeHLNormal()
{
}

void ClientModeHLNormal::Init()
{
	BaseClass::Init();

	gameeventmanager->AddListener( this, "game_newmap", false );

	// Load up the combine control panel scheme
	g_hVGuiCombineScheme = vgui::scheme()->LoadSchemeFromFileEx( enginevgui->GetPanel( PANEL_CLIENTDLL ), IsXbox() ? "resource/ClientScheme.res" : "resource/CombinePanelScheme.res", "CombineScheme" );
	if ( !g_hVGuiCombineScheme )
		Warning( "Couldn't load combine panel scheme!\n" );
}

void ClientModeHLNormal::Shutdown()
{
	if ( BackgroundMovie() )
		BackgroundMovie()->ClearCurrentMovie();
}

void ClientModeHLNormal::InitViewport()
{
	m_pViewport = new CHudViewport();
	m_pViewport->Start( gameuifuncs, gameeventmanager );
}

void ClientModeHLNormal::LevelInit( const char *newmap )
{
	// reset ambient light
	static ConVarRef mat_ambient_light_r( "mat_ambient_light_r" );
	static ConVarRef mat_ambient_light_g( "mat_ambient_light_g" );
	static ConVarRef mat_ambient_light_b( "mat_ambient_light_b" );

	if ( mat_ambient_light_r.IsValid() )
		mat_ambient_light_r.SetValue( "0" );

	if ( mat_ambient_light_g.IsValid() )
		mat_ambient_light_g.SetValue( "0" );

	if ( mat_ambient_light_b.IsValid() )
		mat_ambient_light_b.SetValue( "0" );

	BaseClass::LevelInit( newmap );

	// clear any DSP effects
	CLocalPlayerFilter filter;
	enginesound->SetRoomType( filter, 0 );
	enginesound->SetPlayerDSP( filter, 0, true );
}

void ClientModeHLNormal::LevelShutdown( void )
{
	BaseClass::LevelShutdown();
}

void ClientModeHLNormal::FireGameEvent( IGameEvent *event )
{
	const char *eventname = event->GetName();

	if ( Q_strcmp( "game_newmap", eventname ) == 0 )
	{
		engine->ClientCmd( "exec newmapsettings\n" );
	}
	else
	{
		BaseClass::FireGameEvent(event);
	}
}

void ClientModeHLNormal::Update( void )
{
	UpdatePostProcessingEffects();
}

void ClientModeHLNormal::DoPostScreenSpaceEffects( const CViewSetup *pSetup )
{
	CMatRenderContextPtr pRenderContext( materials );

	C_BaseHLPlayer* pPlayer = dynamic_cast< C_BaseHLPlayer* >( C_BasePlayer::GetLocalPlayer() );
	bool bZoomMode = ( pPlayer != NULL ) && ( pPlayer->IsZoomed() );

	//extern bool g_bRenderingGlows;
	
	if ( bZoomMode )
		DrawSniperScopeStencilMask();

	if ( mat_object_motion_blur_enable.GetBool() )
		DoObjectMotionBlur( pSetup );
	
	// Render object glows and selectively-bloomed objects (under sniper scope)
	//g_bRenderingGlows = true;
	g_GlowObjectManager.RenderGlowEffects( pSetup, GetSplitScreenPlayerSlot() );
	//g_bRenderingGlows = false;

	if ( bZoomMode )
	{
		// Update full frame FB 0 with result of glow pass so it can be used by sniper scope
		ITexture *pFullFrameFB = materials->FindTexture( "_rt_FullFrameFB", TEXTURE_GROUP_RENDER_TARGET );
		pRenderContext->CopyRenderTargetToTexture( pFullFrameFB );
	}
}

void ClientModeHLNormal::OnColorCorrectionWeightsReset( void )
{
	C_ColorCorrection *pNewColorCorrection = NULL;
	C_ColorCorrection *pOldColorCorrection = m_pCurrentColorCorrection;

	C_BaseHLPlayer* pPlayer = dynamic_cast< C_BaseHLPlayer* >( C_BasePlayer::GetLocalPlayer() );
	if ( pPlayer )
		pNewColorCorrection = pPlayer->GetActiveColorCorrection();

	// Only blend between environmental color corrections if there is no failure/infested-induced color correction
	if ( pNewColorCorrection != pOldColorCorrection )
	{
		if ( pOldColorCorrection )
			pOldColorCorrection->EnableOnClient( false );
		if ( pNewColorCorrection )
			pNewColorCorrection->EnableOnClient( true, pOldColorCorrection == NULL );

		m_pCurrentColorCorrection = pNewColorCorrection;
	}
}

void ClientModeHLNormal::DrawSniperScopeStencilMask()
{
	/*extern ConVar asw_sniper_scope_radius;
	CMatRenderContextPtr pRenderContext( materials );
	PIXEvent myEvent( pRenderContext, "SniperScopeStencil" );

	const int NUM_CIRCLE_POINTS = 40;
	Vector2D vScreenSpaceCoordinates[ NUM_CIRCLE_POINTS ];

	int nScreenWidth, nScreenHeight;
	pRenderContext->GetRenderTargetDimensions( nScreenWidth, nScreenHeight );
	Vector2D vScreenSize( nScreenWidth, nScreenHeight );

	// Clear the screen stencil value to 1 to inhibit glows everywhere except in the sniper scope circle, where it will be set to 0
	pRenderContext->ClearStencilBufferRectangle( 0, 0, nScreenWidth, nScreenHeight, 1 );
	float width = asw_sniper_scope_radius.GetFloat() * ( float ) nScreenHeight / 480.0f;
	float height = asw_sniper_scope_radius.GetFloat() * ( float ) nScreenHeight / 480.0f;
	for ( int i = 0; i < NUM_CIRCLE_POINTS; i++ )
	{
		float flAngle = 2.0f * M_PI * ( (float) i / (float) NUM_CIRCLE_POINTS );
		vScreenSpaceCoordinates[i] = Vector2D( nCursorX + width * cos( flAngle ), nCursorY + height * sin( flAngle ) );
		vScreenSpaceCoordinates[i].x = vScreenSpaceCoordinates[i].x / vScreenSize.x * 2.0f - 1.0f;
		vScreenSpaceCoordinates[i].y = 1.0f - vScreenSpaceCoordinates[i].y / vScreenSize.y * 2.0f;
	}		

	ShaderStencilState_t stencilState;
	stencilState.m_bEnable = true;
	stencilState.m_nReferenceValue = 0;
	stencilState.m_CompareFunc = SHADER_STENCILFUNC_ALWAYS;
	stencilState.m_PassOp = SHADER_STENCILOP_SET_TO_REFERENCE;
	stencilState.m_FailOp = SHADER_STENCILOP_KEEP;
	stencilState.m_ZFailOp = SHADER_STENCILOP_SET_TO_REFERENCE;
	pRenderContext->SetStencilState( stencilState );
	
	pRenderContext->OverrideDepthEnable( true, false );
	IMaterial *pMaterial = materials->FindMaterial( "engine/writestencil", TEXTURE_GROUP_OTHER, true );
	DrawNDCSpaceUntexturedPolygon( pMaterial, NUM_CIRCLE_POINTS, vScreenSpaceCoordinates, NULL );
	pRenderContext->OverrideDepthEnable( false, false );*/
}

void ClientModeHLNormal::DoObjectMotionBlur( const CViewSetup *pSetup )
{
	if ( g_ObjectMotionBlurManager.GetDrawableObjectCount() <= 0 )
		return;

	CMatRenderContextPtr pRenderContext( materials );

	ITexture *pFullFrameFB1 = materials->FindTexture( "_rt_FullFrameFB1", TEXTURE_GROUP_RENDER_TARGET );

	//
	// Render Velocities into a full-frame FB1
	//
	IMaterial *pGlowColorMaterial = materials->FindMaterial( "dev/glow_color", TEXTURE_GROUP_OTHER, true );
	
	pRenderContext->PushRenderTargetAndViewport();
	pRenderContext->SetRenderTarget( pFullFrameFB1 );
	pRenderContext->Viewport( 0, 0, pSetup->width, pSetup->height );

	// Red and Green are x- and y- screen-space velocities biased and packed into the [0,1] range.
	// A value of 127 gets mapped to 0, a value of 0 gets mapped to -1, and a value of 255 gets mapped to 1.
	//
	// Blue is set to 1 within the object's bounds and 0 outside, and is used as a mask to ensure that
	// motion blur samples only pull from the core object itself and not surrounding pixels (even though
	// the area being blurred is larger than the core object).
	//
	// Alpha is not used
	pRenderContext->ClearColor4ub( 127, 127, 0, 0 );
	// Clear only color, not depth & stencil
	pRenderContext->ClearBuffers( true, false, false );

	// Save off state
	Vector vOrigColor;
	render->GetColorModulation( vOrigColor.Base() );

	// Use a solid-color unlit material to render velocity into the buffer
	g_pStudioRender->ForcedMaterialOverride( pGlowColorMaterial );
	g_ObjectMotionBlurManager.DrawObjects();	
	g_pStudioRender->ForcedMaterialOverride( NULL );

	render->SetColorModulation( vOrigColor.Base() );
	
	pRenderContext->PopRenderTargetAndViewport();

	//
	// Render full-screen pass
	//
	IMaterial *pMotionBlurMaterial;
	IMaterialVar *pFBTextureVariable;
	IMaterialVar *pVelocityTextureVariable;
	bool bFound1 = false, bFound2 = false;

	// Make sure our render target of choice has the results of the engine post-process pass
	ITexture *pFullFrameFB = materials->FindTexture( "_rt_FullFrameFB", TEXTURE_GROUP_RENDER_TARGET );
	pRenderContext->CopyRenderTargetToTexture( pFullFrameFB );

	pMotionBlurMaterial = materials->FindMaterial( "effects/object_motion_blur", TEXTURE_GROUP_OTHER, true );
	pFBTextureVariable = pMotionBlurMaterial->FindVar( "$fb_texture", &bFound1, true );
	pVelocityTextureVariable = pMotionBlurMaterial->FindVar( "$velocity_texture", &bFound2, true );
	if ( bFound1 && bFound2 )
	{
		pFBTextureVariable->SetTextureValue( pFullFrameFB );
		
		pVelocityTextureVariable->SetTextureValue( pFullFrameFB1 );

		int nWidth, nHeight;
		pRenderContext->GetRenderTargetDimensions( nWidth, nHeight );

		pRenderContext->DrawScreenSpaceRectangle( pMotionBlurMaterial, 0, 0, nWidth, nHeight, 0.0f, 0.0f, nWidth - 1, nHeight - 1, nWidth, nHeight );
	}
}

void ClientModeHLNormal::UpdatePostProcessingEffects()
{
	C_PostProcessController *pNewPostProcessController = NULL;
	C_BaseHLPlayer* pPlayer = dynamic_cast< C_BaseHLPlayer* >( C_BasePlayer::GetLocalPlayer() );
	if ( pPlayer )
		pNewPostProcessController = pPlayer->GetActivePostProcessController();

	// Figure out new endpoints for parameter lerping
	if ( pNewPostProcessController != m_pCurrentPostProcessController )
	{
		m_LerpStartPostProcessParameters = m_CurrentPostProcessParameters;
		m_LerpEndPostProcessParameters = pNewPostProcessController ? pNewPostProcessController->m_PostProcessParameters : PostProcessParameters_t();
		m_pCurrentPostProcessController = pNewPostProcessController;

		float flFadeTime = pNewPostProcessController ? pNewPostProcessController->m_PostProcessParameters.m_flParameters[ PPPN_FADE_TIME ] : 0.0f;
		if ( flFadeTime <= 0.0f )
		{
			flFadeTime = 0.001f;
		}
		m_PostProcessLerpTimer.Start( flFadeTime );
	}

	// Lerp between start and end
	float flLerpFactor = 1.0f - m_PostProcessLerpTimer.GetRemainingRatio();
	for ( int nParameter = 0; nParameter < POST_PROCESS_PARAMETER_COUNT; ++ nParameter )
	{
		m_CurrentPostProcessParameters.m_flParameters[ nParameter ] = 
			Lerp( 
				flLerpFactor, 
				m_LerpStartPostProcessParameters.m_flParameters[ nParameter ], 
				m_LerpEndPostProcessParameters.m_flParameters[ nParameter ] );
	}
	SetPostProcessParams( &m_CurrentPostProcessParameters );
}