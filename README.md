Features
-----------------------

- Source Shader Editor integration
- Support for VS 2010-13 + 2019
- HL2 and HL2MP implementation
- Deferred lighting 
- Fixed Hammer Editor
- Updated engine binaries

Requirements
-----------------------

-MFC Multibyte library for Visual Studio

Contributors
-----------------------

- reepblue
- Joshua Ashton
- DankParrot
- SCell555
- danielmm8888
- biohazard90
- DasMaddi
- Practical Problems
- Ethan